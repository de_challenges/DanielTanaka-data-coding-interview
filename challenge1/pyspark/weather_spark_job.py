from base_spark_job import BaseSparkJob
from pyspark.sql.functions import col, replace
class WeatherSparkJob(BaseSparkJob):
    def transform(self, df):
        for col_name in df.columns:
            df = df.withColumn(col_name, replace(col(col_name), None))
        return df