from base_spark_job import BaseSparkJob
from pyspark.sql import DataFrame

config = {
    "db_host": "localhost",
    "db_port": 5432,
    "db_username": "postgres",
    "db_password": "Password1234**",
    "database": "dw_flights"
}

datasets = {
    'flights': '/home/daniel/git/data-engineer-coding-interview/challenge1/dataset/nyc_flights.csv',
}

class FlightsSparkJob(BaseSparkJob):
    def transform(self, df:DataFrame) -> DataFrame:
        df = df.withColumnRenamed("dep_time", "actual_dep_time") \
               .withColumnRenamed("arr_time", "actual_arr_time")
        df.printSchema()
        return df

if __name__ == '__main__':
    for key in datasets:
        csv_file_location = datasets[key]
        table_destination = key    
        x = BaseSparkJob(csv_file_location=csv_file_location, 
                        table_destination=table_destination,
                        db_host=config["db_host"],
                        db_port=config["db_port"],
                        db_username=config["db_username"],
                        db_password=config["db_password"],
                        database=config["database"])

        x.run()