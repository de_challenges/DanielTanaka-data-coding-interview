from base_spark_job import BaseSparkJob
from pyspark.sql import DataFrame

class FlightsSparkJob(BaseSparkJob):
    def transform(self, df:DataFrame) -> DataFrame:
        return df
