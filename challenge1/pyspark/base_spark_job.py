import pyspark
from pyspark.sql import SparkSession
from pyspark.sql import Row
import os
import os 
config = {
    "db_host": "localhost",
    "db_port": 5432,
    "db_username": "postgres",
    "db_password": "Password1234**",
    "database": "dw_flights"
}

datasets = {
    'airlines_test': '/home/daniel/git/data-engineer-coding-interview/challenge1/dataset/nyc_airlines.csv',
    'airports_test': '/home/daniel/git/data-engineer-coding-interview/challenge1/dataset/nyc_airports.csv',
    'flights_test': '/home/daniel/git/data-engineer-coding-interview/challenge1/dataset/nyc_flights.csv',
    'planes_test': '/home/daniel/git/data-engineer-coding-interview/challenge1/dataset/nyc_planes.csv',
    'weather_test': '/home/daniel/git/data-engineer-coding-interview/challenge1/dataset/nyc_weather.csv'
}

datasets = {
    'flights_test': '/home/daniel/git/data-engineer-coding-interview/challenge1/dataset/nyc_flights.csv',
}


class BaseSparkJob:
    def __init__(self,
                 csv_file_location, 
                 table_destination,
                 db_host,
                 db_port,
                 db_username,
                 db_password,
                 database):
        self.csv_file_location = csv_file_location
        self.table_destination = table_destination
        self.db_host = db_host
        self.db_port = db_port
        self.db_username = db_username
        self.db_password = db_password
        self.database = database
        self.spark = self.get_spark_session()


    @staticmethod
    def get_spark_session():
        path = os.path.split(os.path.realpath(__file__))[0]

        jar_path = f"{path}/jars/postgresql-42.2.5.jar"
        spark = SparkSession.builder. \
                config("spark.jars", jar_path). \
                master("local").appName("de_challenge").getOrCreate()
        return spark
    
    def extract(self):
        df = self.spark.read.option("header", True).option("inferschema","true").csv(self.csv_file_location)
        df.show(n=5)
        df.printSchema()
        return df

    @staticmethod
    def transform(df):
        df = df.drop("_c0")
        df.printSchema()
        return df

    def load(self, df):
        url = f"jdbc:postgresql://{self.db_host}:{self.db_port}/{self.database}"
        user = self.db_username
        password = self.db_password
        properties = {
                        "user": user,
                        "password": password,
                        "driver": "org.postgresql.Driver"
                     }
        df.write.jdbc(url, self.table_destination, mode="overwrite", properties=properties)

    def run(self):
        df = self.extract()
        df_transformed = self.transform(df=df)
        self.load(df=df_transformed)

if __name__ == '__main__':
    for key in datasets:
        csv_file_location = datasets[key]
        table_destination = key    
        x = BaseSparkJob(csv_file_location=csv_file_location, 
                        table_destination=table_destination,
                        db_host=config["db_host"],
                        db_port=config["db_port"],
                        db_username=config["db_username"],
                        db_password=config["db_password"],
                        database=config["database"])

        x.run()
